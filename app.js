var express = require("express");
var bodyParser = require("body-parser");
var app = express();

//Should we want to do JSON requests in the future
//app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

//API endpoints go here. 
var routes = require("./routes/routes.js")(app);

var server = app.listen(8080, function () {
    console.log("Server started, listening on port %s", server.address().port);
});

//for testing
module.exports = server;
