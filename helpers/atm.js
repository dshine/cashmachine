//array of available notes. Notes should be in descending order. New notes can be added.
// === Future feature, sort notes array in descending order to avoid coding changes/errors ===
var notes = [100,50,20,10];

//Withdraw does error checking and returns notes breakdown if available. Function is reusable.
var withdraw = function(amount){
	var data = [];
	try{
		//amount is blank
    	if(!amount) {
	        return data;
	    //
	    }else if (isNaN(amount) || amount < 0 ){
	    	var err = new Error('InvalidArgumentException');
			throw err
		}else {
			//If amount divided by lowest note has a remainder or amount is zero 
			if (amount % notes[notes.length -1] || amount == 0){
				var err = new Error('NoteUnavailableException');
				throw err;
			}else{
				for(var i=0; i < notes.length; i++ ){
					if (amount / notes[i] >= 1 ){
						for(var j=0; j<Math.floor(amount / notes[i]); j++ ){
							data.push(notes[i] + ".00");
						}
					}
					//set amount equal to remainder
					amount = amount % notes[i];
				}
				//had to add [ ] to get the data to display in the format required
				data = '['+ data + ']';
				return data;
			}
		}	    	
    }catch (err){
    	return err.message;
    }
}

exports.withdraw = withdraw;