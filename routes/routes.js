var atm = require("../helpers/atm.js")

var appRouter = function(app, notes) {

	app.get("/", function(req, res) {
    	res.send("Welcome to the project");
	});

	app.get("/withdraw", function(req, res) {
	    var amount = req.query.amount;
	    res.send(atm.withdraw(amount));
	});
}

module.exports = appRouter;