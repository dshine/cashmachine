var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app.js');
var should = chai.should();

chai.use(chaiHttp);

describe("ATM API", function(){
	// How long to wait for a response (ms)
	this.timeout(5000); 

	before(function() {

	});

	after(function() {

	});
	//send empty amount
	it("/withdraw?amount= | send empty amount", function(done){
		chai.request(server)
		.get('/withdraw?amount=')
		.end(function(err, res){
			res.should.have.status(200);
			res.body.should.be.a('array');
			res.body.should.be.empty;
			done();
		});
	});
	//send negative amount
	it("/withdraw?amount=-200 | send negative amount", function(done){
		chai.request(server)
		.get('/withdraw?amount=-200')
		.end(function(err, res){
			res.should.have.status(200);
			res.text.should.equal('InvalidArgumentException');
			done();
		});
	});
	//send incorrect amount for notes
	it("/withdraw?amount=24 | send incorrect amount to generate note unavailable", function(done){
		chai.request(server)
		.get('/withdraw?amount=24')
		.end(function(err, res){
			res.should.have.status(200);
			res.text.should.equal('NoteUnavailableException');
			done();
		});
	});
	//send string instead of number amount
	it("/withdraw?amount=tenthousandeuro | send string instead of numbers", function(done){
		chai.request(server)
		.get('/withdraw?amount=tenthousandeuro')
		.end(function(err, res){
			res.should.have.status(200);
			//res.body.should.be.a('string');
			res.text.should.equal('InvalidArgumentException');
			done();
		});
	});
	//send correct value
	it("/withdraw?amount=580 | send correct amount", function(done){
		chai.request(server)
		.get('/withdraw?amount=580')
		.end(function(err, res){
			res.should.have.status(200);
			res.text.should.not.be.empty;
			done();
		});
	});
});